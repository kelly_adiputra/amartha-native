package com.project.amartha

import android.annotation.SuppressLint
import android.content.Intent
import android.provider.CalendarContract
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import java.text.SimpleDateFormat



class MainAdapter(private val events: List<Questions>, private val listener: (Questions) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var questions: List<Questions>? = null
    var question: Questions? = null

    fun MainAdapter(consolidatedList: List<Questions>) {
        setConsolidatedList(consolidatedList)
    }


    fun setConsolidatedList(consolidatedList: List<Questions>?) {
            this.questions = consolidatedList

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.getContext())

        when (viewType) {

            0 -> {
                val v1 = inflater.inflate(
                    R.layout.item_email, parent,
                    false
                )
                viewHolder = EmailViewHolder(v1)
            }

            1 -> {
                val v2 = inflater.inflate(R.layout.item_email, parent, false)
                viewHolder = EmailViewHolder(v2)
            }
        }

        return viewHolder!!
    }

    override fun getItemViewType(position: Int): Int {

        if (questions!!.get(position).typeField == "email") {
            return 0;
        } else if (questions!!.get(position).typeField == "password") {
            return 1;
        } else if (questions!!.get(position).typeField == "text") {
            return 2;
        } else if (questions!!.get(position).typeField == "date") {
            return 3;
        } else if (questions!!.get(position).typeField == "select") {
            return 4;
        }
        return 0;
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.getItemViewType()) {

            0 -> {
                question = questions!!.get(position)

//                val generalItem = consolidatedList.get(position) as GeneralItem
//                val generalViewHolder = newsViewHolder as NewsViewHolder
//                Glide.with(generalViewHolder.itemView).load(generalItem.getArticle().getUrlToImage())
//                    .into(generalViewHolder.imageView)
//                generalViewHolder.tvTitle.setText(generalItem.getArticle().getTitle())
//                generalViewHolder.tvDescription.setText(generalItem.getArticle().getDescription())
//
//                val newDate = DateTime.formatDate(generalItem.getArticle().getPublishedAt())
//                generalViewHolder.tvDate.setText(newDate)
            }

            1 -> {
                question = questions!!.get(position)

//                val dateItem = consolidatedList.get(position) as DateItem
//                val dateViewHolder = newsViewHolder as DateViewHolder
//
//                dateViewHolder.txtTitle.setText(dateItem.getDate())
            }
        }// Populate date item data here
    }

    override fun getItemCount(): Int = events.size

    class EmailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var labelText: TextView = itemView.findViewById(R.id.labelText)
        private var errorText: TextView  = itemView.findViewById(R.id.errorText)
        private var editText: EditText  = itemView.findViewById(R.id.editText)


    }
}

//
//class EventUI : AnkoComponent<ViewGroup> {
//    override fun createView(ui: AnkoContext<ViewGroup>): View {
//
//        return with(ui) {
//            themedCardView(R.style.AppTheme_NoActionBar) {
//                lparams {
//                    height = wrapContent
//                    width = matchParent
//                    verticalMargin = dip(5)
//                    horizontalMargin = dip(10)}
//
//                linearLayout {
//                    lparams(width = matchParent, height = wrapContent)
//                    orientation = android.widget.LinearLayout.VERTICAL
//                    gravity = android.view.Gravity.CENTER_HORIZONTAL
//
//                    linearLayout {
//                        lparams(width = matchParent, height = wrapContent)
//                        orientation = android.widget.LinearLayout.HORIZONTAL
//                        gravity = android.view.Gravity.CENTER_HORIZONTAL
//
//                        textView {
//                            id = R.id.event_date
//                            textSize = 16f
//                        }.lparams {
//                            topMargin = dip(10)
//                            bottomMargin = dip(5)
//                        }
//
//                        floatingActionButton {
//                            imageResource = R.drawable.ic_event_black_24dp
//
//                            id = R.id.add_To_Calendar
//                        }.lparams(width= matchParent, height = wrapContent){
//                            gravity = android.view.Gravity.END
//
//
//                        }
//
//                    }
//
//                    linearLayout {
//                        lparams(width = matchParent, height = wrapContent)
//                        orientation = android.widget.LinearLayout.HORIZONTAL
//
//                        linearLayout {
//                            lparams(width = 0, height = wrapContent)
//                            {
//                                weight = 3F
//                                leftMargin = dip(20)
//                                orientation = android.widget.LinearLayout.VERTICAL
//                            }
//                            gravity = android.view.Gravity.CENTER
//                            imageView {
//                                id = R.id.homeImg
//                            }.lparams(width = dip(50), height = dip(50)){
//
//                                gravity = android.view.Gravity.CENTER
//                            }
//
//                            textView {
//                                id = R.id.region_name
//                                textSize = 16f
//                            }.lparams {
//                                margin = dip(5)
//                                width = wrapContent
//                            }
//                        }
//                        linearLayout {
//                            lparams(width = 0, height = wrapContent)
//                            { orientation = android.widget.LinearLayout.VERTICAL
//                                weight = 2F}
//                            gravity = android.view.Gravity.CENTER_HORIZONTAL
//
//                            textView {
//                                id = R.id.event_time
//                                textSize = 16f
//                            }.lparams {
//                            }
//
//                            linearLayout {
//                                lparams(width = matchParent, height = matchParent)
//                                { orientation = android.widget.LinearLayout.HORIZONTAL }
//                                linearLayout {
//                                    lparams(width = 0, height = wrapContent)
//                                    { orientation = android.widget.LinearLayout.VERTICAL
//                                        weight = 0.5F }
//                                    gravity = android.view.Gravity.CENTER_HORIZONTAL
//
//                                    textView {
//                                        id = R.id.region_score
//                                        textSize = 16f
//                                        typeface = android.graphics.Typeface.DEFAULT_BOLD
//                                    }.lparams {
//                                        margin = dip(5)
//                                        width = wrapContent
//                                    }
//                                }
//                                linearLayout {
//                                    lparams(width = 0, height = wrapContent)
//                                    {
//                                        weight = 1F
//                                        orientation = android.widget.LinearLayout.VERTICAL
//                                    }
//                                    gravity = android.view.Gravity.CENTER_HORIZONTAL
//
//                                    textView {
//                                        id = R.id.versus_name
//                                        textSize = 16f
//                                    }.lparams {
//                                        margin = dip(5)
//                                        width = wrapContent
//                                    }
//                                }
//
//                                linearLayout {
//                                    lparams(width = 0, height = wrapContent)
//                                    { weight = 0.5F }
//                                    gravity = android.view.Gravity.CENTER_HORIZONTAL
//
//                                    textView {
//                                        id = R.id.versus_region_score
//                                        textSize = 16f
//                                        typeface = android.graphics.Typeface.DEFAULT_BOLD
//                                    }.lparams {
//                                        margin = dip(5)
//                                        width = wrapContent
//
//
//                                    }
//                                }
//                            }
//                        }
//
//                        linearLayout {
//                            lparams(width = 0, height = wrapContent)
//                            {
//                                weight = 3F
//                                rightMargin = dip(20)
//                                orientation = android.widget.LinearLayout.VERTICAL
//                            }
//
//                            gravity = android.view.Gravity.CENTER_HORIZONTAL
//                            imageView {
//                                id = R.id.awayImg
//                            }.lparams(width = dip(50), height = dip(50)){
//
//                                gravity = android.view.Gravity.CENTER
//                            }
//
//                            textView {
//                                id = R.id.versus_region_name
//                                textSize = 16f
//                            }.lparams {
//                                margin = dip(5)
//                                width = wrapContent
//                            }
//                        }
//                    }
//
//                }
//            }
//        }
//    }
//
//}
//
//class EventViewHolder(view: View) : RecyclerView.ViewHolder(view){
//
//    private val eventDate: TextView = view.find(event_date)
//    private val eventTime: TextView = view.find(event_time)
//    private val addToCalendar: FloatingActionButton = view.find(add_To_Calendar)
//    private val regionName: TextView = view.find(region_name)
//    private val regionScore: TextView = view.find(region_score)
//    private val versusName: TextView = view.find(versus_name)
//    private val versusRegionName: TextView = view.find(versus_region_name)
//    private val versusRegionScore: TextView = view.find(versus_region_score)
//    private val homeImage: ImageView = view.find(homeImg)
//    private val awayImage: ImageView = view.find(awayImg)
//
//
//    fun bindItem(events: Event, listener: (Event) -> Unit) {
//
//        regionName.text = events.strHomeTeam
//        regionScore.text = events.intHomeScore
//        versusName.text = "VS"
//        versusRegionName.text = events.strAwayTeam
//        versusRegionScore.text = events.intAwayScore
//
//        if(events.strHomeBadge != null && events.strHomeBadge != "") {
//            Picasso.get().load(events.strHomeBadge).into(homeImage)
//        }
//        if(events.strAwayBadge != null && events.strAwayBadge != "") {
//            Picasso.get().load(events.strAwayBadge).into(awayImage)
//        }
//        itemView.onClick {
//            listener(events)
//        }
//        val DateNow = System.currentTimeMillis()
//        val Format = SimpleDateFormat("yyyy-MM-dd")
//
//        val TimeFormat = SimpleDateFormat("HH:mm")
//        val DateFormated = Format.format(DateNow)
//        val DifferentDate = events.dateEvent!!.compareTo(DateFormated)
//        val dateGMTFormat = DateTime.changeToGMTFormat(events.dateEvent, events.strTime)
//
//        eventDate.text = DateTime.getLongDate(Format.format(dateGMTFormat))
//        eventTime.text = TimeFormat.format(dateGMTFormat)
//
//        // show addToCalendar Button by Match Date Past Current Date
//        when {
//            DifferentDate != null && DifferentDate >= 0 -> {
//                addToCalendar.visibility = View.VISIBLE
//                addToCalendar.setOnClickListener {
//                    val intent = Intent(Intent.ACTION_EDIT)
//                    intent.type = "vnd.android.cursor.item/event"
//                    intent.putExtra(CalendarContract.Events.TITLE, events.strEvent)
//                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, dateGMTFormat)
//                    intent.putExtra(CalendarContract.Events.ALL_DAY, false)
//                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, dateGMTFormat)
//                    intent.putExtra(CalendarContract.Events.DESCRIPTION, events.strEvent)
//                    itemView.context.startActivity(intent)
//                }
//            }
//            else -> {
//                addToCalendar.visibility = View.INVISIBLE
//            }
//        }
//    }
//
//}
