package com.project.amartha

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Question(val questions : List<Questions>)
data class Questions(
    @SerializedName("key")
    @Expose
    val key: String,
    @SerializedName("label")
    @Expose
    val label: String,
    @SerializedName("typeField")
    @Expose
    val typeField: String,
    @SerializedName("validation")
    @Expose
    val validation: Validation?,
    @SerializedName("options")
    @Expose
    val options: List<Option>? = null)

data class Option(
    @SerializedName("label")
    @Expose
    val label: String,
    @SerializedName("value")
    @Expose
    val value: String)

data class Validation(
    @SerializedName("required")
    @Expose
    val required: Boolean,
    @SerializedName("minLength")
    @Expose
    val minLength: Int?,
    @SerializedName("exactLength")
    @Expose
    val exactLength: Int?)