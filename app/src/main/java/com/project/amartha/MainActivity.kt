package com.project.amartha

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.opengl.ETC1.getWidth
import android.content.Context.WINDOW_SERVICE
import android.graphics.Color
import android.support.v4.content.ContextCompat.getSystemService
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.WindowManager
import android.view.Display
import android.view.View
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import org.w3c.dom.Text
import java.io.IOException
import java.io.InputStreamReader
import java.net.URL
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity() {

    private lateinit var QuestionFeed      : Question
    private lateinit var ll : LinearLayout
    private lateinit var generalError: TextView
    private lateinit var submitButton: Button
    private lateinit var Result: HashMap<String, String>

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ll = findViewById(R.id.list_Questions) as LinearLayout
        generalError = findViewById(R.id.generalError) as TextView
        submitButton = findViewById(R.id.btn_submit) as Button
        Result = HashMap()
        val url ="https://raw.githubusercontent.com/amarthaid/test-mobile/master/Problem-1/question.json"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback{
            override fun onResponse(call: Call, response: Response) {
                val body = response?.body()?.string()
                val gson = GsonBuilder().create()
                QuestionFeed = gson.fromJson(body, Question::class.java)
                runOnUiThread{
                    initData()
                }
            }
            override fun onFailure(call: Call, e: IOException) {
            }
        })
        submitButton.setOnClickListener(View.OnClickListener { Validate() })

    }
    private fun Validate(){
        if(Result.size != QuestionFeed.questions.size){
            var error = "";
            for(i in 0..QuestionFeed.questions.size-1){
                if(Result.get(QuestionFeed.questions[i].key)== null && QuestionFeed.questions[i].validation!=  null
                    && QuestionFeed.questions[i].validation!!.required){
                    error += QuestionFeed.questions[i].key + " is required . Please fill it \n"
                }
            }
            if(error ==""){
                // show answer using dialog
                val builder = AlertDialog.Builder(this)
                builder.setTitle("RESULT !!")
                builder.setMessage(error)
                val dialog = builder.create()
                dialog.show()
            }else{
                generalError.visibility = View.VISIBLE
                generalError.setText(error)
                var result = ""
                for(x in Result.keys) {
                    result += x + " : " + Result.get(x) + "\n"
                }

                val builder = AlertDialog.Builder(this)
                builder.setTitle("RESULT !!")
                builder.setMessage(result)
                val dialog = builder.create()
                dialog.show()
                // show error on general error

            }
        }else{
            var result = ""
            for(x in Result.keys) {
                result += x + " : " + Result.get(x) + "\n"
            }

            val builder = AlertDialog.Builder(this)
            builder.setTitle("RESULT !!")
            builder.setMessage(result)
            val dialog = builder.create()
            dialog.show()
        }
    }

    private fun initData() {
        val display = (applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        val width = display.width

        for (i in 0..QuestionFeed.questions.size - 1) {
          when(QuestionFeed.questions[i].typeField){
              "email"-> initText(QuestionFeed.questions[i], "email")
              "password"-> initText(QuestionFeed.questions[i], "password")
              "text"-> initText(QuestionFeed.questions[i])
              "date" -> initDatePicker(QuestionFeed.questions[i])
              "select" -> initSelect(QuestionFeed.questions[i])
          }
        }
    }
    private fun initText(question : Questions, type: String = "default"){
        val l = LinearLayout(this)
        l.orientation = LinearLayout.HORIZONTAL
        val tv = TextView(this)
        tv.text=question.label + " :   "
        l.addView(tv)
        val editText = EditText(this)
        if(type == "password"){
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        l.addView(editText)

        this.ll.addView(l)
        val error = LinearLayout(this)
        error.orientation = LinearLayout.HORIZONTAL
        val errorText = TextView(this)
        errorText.setTextColor(Color.RED)
        error.addView(errorText)
        error.visibility = View.GONE
        l.addView(error)
        editText?.setOnFocusChangeListener(View.OnFocusChangeListener{view, hasFocus ->
            if(!hasFocus && question.validation != null && question.validation.required){
                if(editText.text.toString() == "" ){
                    error.visibility = View.VISIBLE
                    errorText.text = "this field is required"
                }else if(question.validation.minLength != null && question.validation.minLength > editText.text.length){
                    error.visibility = View.VISIBLE
                    errorText.text = "this field must have minimum length " + question.validation.minLength + "words"
                }else if(question.validation.exactLength != null && question.validation.exactLength != editText.text.length){
                    error.visibility = View.VISIBLE
                    errorText.text = "this field must have minimum length " + question.validation.exactLength + "words"
                }else{
                    error.visibility = View.GONE
                }
            }
        })

    }
    private fun initText(question : Questions){
        val l = LinearLayout(this)
        l.orientation = LinearLayout.HORIZONTAL
        val tv = TextView(this)
        tv.text=question.label + " :   "
        l.addView(tv)
        val editText = EditText(this)
        l.addView(editText)

        this.ll.addView(l)
        val error = LinearLayout(this)
        error.orientation = LinearLayout.HORIZONTAL
        val errorText = TextView(this)
        errorText.setTextColor(Color.RED)
        error.addView(errorText)
        error.visibility = View.GONE
        l.addView(error)
        editText?.setOnFocusChangeListener(View.OnFocusChangeListener{view, hasFocus ->
            if(!hasFocus && question.validation != null && question.validation.required){
                if(editText.text.toString() == "" ){
                    error.visibility = View.VISIBLE
                    errorText.text = "this field is required"
                }else if(question.validation.minLength != null && question.validation.minLength > editText.text.length){
                    error.visibility = View.VISIBLE
                    errorText.text = "this field must have minimum length " + question.validation.minLength + "words"
                }else if(question.validation.exactLength != null && question.validation.exactLength != editText.text.length){
                    error.visibility = View.VISIBLE
                    errorText.text = "this field must have exact length " + question.validation.exactLength + "words"
                }else{
                    Result.put(question.key,editText.text.toString())
                    error.visibility = View.GONE
                }
            }else{
                error.visibility = View.GONE
            }
        })

    }

    private fun initSelect(question : Questions){
        val l = LinearLayout(this)
        l.orientation = LinearLayout.HORIZONTAL
        val tv = TextView(this)
        tv.text=question.label + " :   "
        l.addView(tv)
        val radioGroup = RadioGroup(this)
        for(i in 0..question.options!!.size - 1) {
            val radioText1 = RadioButton(this)
            radioText1.text = question.options[i].label
            radioText1.id = i

            radioGroup.addView(radioText1)
        }
        radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->

            Result.put(question.key,question.options[i-1].label)
        })

        l.addView(radioGroup)
        this.ll.addView(l)
    }

    private fun initDatePicker(question : Questions){
        val l = LinearLayout(this)
        l.orientation = LinearLayout.HORIZONTAL
        val dateTxt = TextView(this)
        val tv = TextView(this)
        tv.text=question.label + " :   "
        dateTxt!!.text = "--/--/----"
        l.addView(tv)
        l.addView(dateTxt)
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {


                val myFormat = "MM/dd/yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                Log.d("year",year.toString())
                val calendar = Calendar.getInstance()
                calendar.set(year,monthOfYear,dayOfMonth)
                dateTxt.text= sdf.format(calendar.time)
                Result.put(question.key,sdf.format(calendar.time))
            }
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        dateTxt!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                val myTextView = view as TextView;
                var value = myTextView.text as String
                var InputDate = Date()
                if(value != "--/--/----"){

                    val format = SimpleDateFormat("MM/dd/yyyy")
                    format.timeZone = TimeZone.getTimeZone("UTC")
                    InputDate= format.parse(value)
                }
                val calendar = Calendar.getInstance()
                calendar.time = InputDate
                Log.d("year", InputDate.toString())
                Log.d("year", InputDate.year.toString())
                DatePickerDialog(this@MainActivity,
                    dateSetListener,

                    // set DatePickerDialog to point to today's date when it loads up
                    calendar.get(Calendar.YEAR),
                    InputDate.month,
                    InputDate.date).show()
            }

        })
        this.ll.addView(l)

    }


}


